from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import MessageSettings, Message


@admin.register(MessageSettings)
class CareerAdmin(SingletonModelAdmin):
    pass


@admin.register(Message)
class CareerAdmin(admin.ModelAdmin):
    list_display = ('result', 'percent', 'message')
    list_display_links = ('result', 'percent', 'message')
    list_filter = ('result',)
