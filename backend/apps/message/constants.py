# Эмоциональный окрас сообщения
NEUTRAL = 0  # Нейтральный
POSITIVE = 1  # Положиельный
NEGATIVE = 2  # Отрицательный

MESSAGE_COLOR_TEXT = {
    NEUTRAL: 'Нейтральное',
    POSITIVE: 'Позитивное',
    NEGATIVE: 'Негативное',
}

# Участники диалога
AGENT = 'AGENT'
CLIENT = 'CLIENT'
BOT = 'BOT'
