from django.db import models
from solo.models import SingletonModel


class Message(models.Model):

    message = models.TextField('Сообщение')
    result = models.IntegerField('Результат классификации', default=0)
    percent = models.IntegerField('Вероятность', default=0)


class MessageSettings(SingletonModel):

    finodays_excel = models.FileField(upload_to='documents')
    example_excel = models.FileField(upload_to='documents')
