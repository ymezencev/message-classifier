from django.shortcuts import render
from django.views.generic import TemplateView

from .constants import MESSAGE_COLOR_TEXT
from .forms import MessagreForm, ExcelMessagreForm
from .models import Message, MessageSettings

from .services import MessageColorService


class IndexMessageView(TemplateView):
    template_name = 'message/index.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['message_settings'] = MessageSettings.get_solo()
        return ctx

    def post(self, request):

        form = MessagreForm(self.request.POST)
        category, percent = None, None
        if form.is_valid():
            data = form.cleaned_data
            message_text = data.get('message')
            service = MessageColorService()
            category, percent = service.get_message_color(msg=message_text)
            message = Message(message=message_text, result=category, percent=percent)
            message.save()

        ctx = self.get_context_data()
        category = MESSAGE_COLOR_TEXT.get(category)
        ctx.update({'category': category, 'percent': percent})
        return render(request, self.template_name, ctx)


class ExcelMessageView(IndexMessageView):

    def post(self, request):
        form = ExcelMessagreForm(self.request.POST, self.request.FILES)
        excel_result = None
        if form.is_valid():
            excel_file = self.request.FILES['excel_file']
            service = MessageColorService()
            excel_result = service.get_message_color_from_excel(file=excel_file)
        ctx = self.get_context_data()
        ctx.update({'excel_result': excel_result})
        return render(request, self.template_name, ctx)
