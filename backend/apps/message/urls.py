from django.urls import path

from . import views

app_name = 'message'
urlpatterns = [
    path('', views.IndexMessageView.as_view(), name='index-message'),
    path('message/excel', views.ExcelMessageView.as_view(), name='excel-message'),
]
