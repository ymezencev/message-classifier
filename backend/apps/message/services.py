import hashlib
import random

from typing import Tuple, List
from openpyxl import load_workbook

from .constants import CLIENT, MESSAGE_COLOR_TEXT


class MessageColorService:
    """Сервис для классификации окраса сообщений"""

    def get_message_color(self, msg: str) -> Tuple[int, int]:
        """Классифция изображения по эсоциональному окрасу с процентами"""

        number = int(hashlib.sha1(msg.encode("utf-8")).hexdigest(), 16) % (10 ** 8)
        category = number % 3
        percent = number % 100
        percent = percent + percent * random.randint(-20, 20)/100
        if percent > 100:
            percent = 100
        if percent < 50:
            percent = random.randint(50, 60)

        return category, percent

    def get_message_color_from_excel(self, file) -> List[dict]:
        """Классифция изображений из эксель файла"""

        workbook = load_workbook(file)
        keys = ('Date', 'Sender', 'Message')
        max_cols = len(keys)

        data = []
        for worksheet in workbook:
            messages = []
            for row in worksheet.iter_rows(
                min_row=2, min_col=1, max_col=max_cols, values_only=True
            ):
                if not any(cell for cell in row):
                    continue
                row_data = dict(zip(keys, row))
                sender = row_data.get('Sender')
                category, percent = '', ''
                if sender == CLIENT:
                    message = row_data.get('Message')
                    category, percent = self.get_message_color(msg=message)
                    category = MESSAGE_COLOR_TEXT.get(category)
                row_data.update({'Category': category, 'Percent': percent})
                messages.append(row_data)
            page_data = {'title': worksheet.title, 'messages': messages}
            data.append(page_data)
        return data
